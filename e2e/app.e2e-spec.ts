import {Agroprint} from './app.po';

describe('agroprint.frontend App', () => {
	let page: Agroprint;

	beforeEach(() => {
		page = new Agroprint();
	});

	it('should display message saying app works', () => {
		page.navigateTo();
		expect(page.getParagraphText()).toEqual('app works!');
	});
});
