import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';

import {AppComponent} from './app.component';
import {KnifesComponent} from './components/knifes/knifes.component';
import {ShapesComponent} from './components/shapes/shapes.component';
import {MaterialsComponent} from './components/materials/materials.component';
import {PolishesComponent} from './components/polishes/polishes.component';
import {ColorsComponent} from './components/colors/colors.component';
import {PantonesComponent} from './components/pantones/pantones.component';
import {QuantitiesComponent} from './components/quantities/quantities.component';
import {KindsComponent} from './components/kinds/kinds.component';
import {ResultComponent} from './components/result/result.component';
import {OrderComponent} from './components/order/order.component';
import {CalculatorComponent} from './components/calculator/calculator.component';

import {ColorsService} from './services/colors.service';
import {ShapesService} from './services/shapes.service';
import {CalculatorService} from './services/calculator.service';
import {ThanksComponent} from './components/thanks/thanks.component';

@NgModule({
	declarations: [
		AppComponent,
		KnifesComponent,
		ShapesComponent,
		MaterialsComponent,
		PolishesComponent,
		ColorsComponent,
		PantonesComponent,
		QuantitiesComponent,
		KindsComponent,
		ResultComponent,
		OrderComponent,
		CalculatorComponent,
		ThanksComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule,
		RouterModule.forRoot([
			{path: '', redirectTo: '/calculator', pathMatch: 'full'},
			{path: 'calculator', component: CalculatorComponent},
			{path: 'order', component: OrderComponent},
			{path: 'thanks', component: ThanksComponent},
		], {useHash: true})
	],
	providers: [ColorsService, ShapesService, CalculatorService],
	bootstrap: [AppComponent]
})
export class AppModule {
}
