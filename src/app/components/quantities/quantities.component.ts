import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {QuantitiesService} from '../../services/quantities.service';

@Component({
	selector: 'app-quantities',
	templateUrl: './quantities.component.html',
	styleUrls: ['./quantities.component.css']
})
export class QuantitiesComponent implements OnInit {

	label = 'Общий тираж';

	@Output() selectedQuantity = new EventEmitter();

	quantities: IQuantityStructure[] = [];

	constructor(private quantitiesService: QuantitiesService) {
	}

	ngOnInit() {

		this.getQuantities();
	}

	private getQuantities(): void {

		this.quantities = this.quantitiesService.getQuantities();
		this.quantitiesService.selectedQuantity = {
			id: this.quantities[0].value.toString(),
			label: this.quantities[0].value.toString(),
			value: this.quantities[0].value
		};
	}

	/**
	 * Функция изменения выбранного пантона
	 * @param selectedQuantity
	 */
	quantityChange(selectedQuantity: string): void {

		this.selectedQuantity.emit(parseInt(selectedQuantity, 10));
		this.quantitiesService.selectedQuantity = {id: selectedQuantity, label: selectedQuantity, value: parseInt(selectedQuantity, 10)};
	}
}
