/// <reference path='../../pantones.d.ts' />

import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {PantonesService} from '../../services/pantones.service';

@Component({
	selector: 'app-pantones',
	templateUrl: './pantones.component.html',
	styleUrls: ['./pantones.component.css']
})
export class PantonesComponent implements OnInit, OnChanges {

	label = 'из них пантонов';
	@Input() baseColor: number;
	@Output() selectedPantone = new EventEmitter();

	/**
	 * Массив с цветами
	 * @type {Array}
	 */
	pantones: IPantoneStructure[] = [];

	constructor(private pantonesService: PantonesService) {

	}

	ngOnInit() {

		this.getPantones();
	}

	/**
	 * Функция отслеживает состояние входной переменной с количеством цветов.
	 * В случае изменения - производит обновление списка пантонов
	 * @param changes
	 */
	ngOnChanges(changes: SimpleChanges) {

		if (changes) {

			for (const propName in changes) {

				if (propName === 'baseColor') {
					this.getPantones();
				}
			}
		}
	}

	private getPantones(): void {

		this.pantones = this.pantonesService.getPantones(this.baseColor);
		this.pantonesService.selectedPantone = {id: this.baseColor.toString(), label: this.baseColor.toString(), value: this.baseColor};
	}

	/**
	 * Функция изменения выбранного пантона
	 * @param selectedPantone
	 */
	pantoneChange(selectedPantone: string): void {

		this.selectedPantone.emit(parseInt(selectedPantone, 10));
		this.pantonesService.selectedPantone = {id: selectedPantone, label: selectedPantone, value: parseInt(selectedPantone, 10)};
	}
}
