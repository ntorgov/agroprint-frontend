import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PantonesComponent} from './pantones.component';

describe('PantonesComponent', () => {
	let component: PantonesComponent;
	let fixture: ComponentFixture<PantonesComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [PantonesComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PantonesComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
