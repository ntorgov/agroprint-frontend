/// <reference path='../../models/order.d.ts' />

import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {CalculatorService} from '../../services/calculator.service';
import {Order} from '../../models/order';
import {Calculator} from '../../models/calculator';

@Component({
	selector: 'app-result',
	templateUrl: './result.component.html',
	styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit, OnChanges {

	calculator: Calculator;

	@Input() orderData: Order;
	@Input() shape: string;
	@Input() knife: string;
	@Input() material: string;
	@Input() polish: string;
	@Input() colors: number;
	@Input() pantones: number;
	@Input() kinds: number;
	@Input() quantity: number;
	@Output() calculatorData: EventEmitter<ICalculatorStructure> = new EventEmitter();

	errorMessage = '';

	constructor(private calculatorService: CalculatorService) {
	}

	ngOnInit() {
		this.calculator = {
			TotalCost: '0',
			StickerCost: '0',
			TotalTime: '0',
			TotalLength: '0',
			TotalSquare: '0',
			TimeToReroll: '0',
			ClearLength: '0',
			SecondPrintingCost: '0',
			SecondPrintingStickerCost: '0',
			FlexFormCost: '0',
			TotalWeight: '0'
		};
	}

	/**
	 * Функция отслеживает состояние входной переменной с формой ножа
	 * В случае изменения - производит обновление списка ножей
	 * @param changes
	 */
	ngOnChanges(changes: SimpleChanges) {

		if (changes) {

			this.getOrder();
		}
	}

	private getOrder() {

		if (this.orderData.knife && this.orderData.material) {

			const newOrderData: Order = {
				shape: this.shape,
				colors: this.colors,
				pantones: this.pantones,
				polish: this.polish,
				kinds: this.kinds,
				material: this.material,
				quantity: this.quantity,
				knife: this.knife
			};
			this.calculatorService.getOrder(newOrderData).subscribe(
				data => this.calculator = data,
				error => this.errorMessage = <any>error,
				() => {
					this.calculator.TotalCost = parseFloat(this.calculator.TotalCost).toFixed(2);
					this.calculator.StickerCost = parseFloat(this.calculator.StickerCost).toFixed(2);
					this.calculatorData.emit(this.calculator);
					this.calculatorService.calculatorData = this.calculator;
				});
		}
	}

}
