/// <reference path='../../colors.d.ts' />

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ColorsService} from '../../services/colors.service';

@Component({
	selector: 'app-colors',
	templateUrl: './colors.component.html',
	styleUrls: ['./colors.component.css'],
})
export class ColorsComponent implements OnInit {

	label = 'Количество цветов';
	@Input() baseColor: number;
	@Output() selectedColor = new EventEmitter();

	/**
	 * Цвет по умолчанию
	 * @type {number}
	 */
	public defaultColor = 1;

	/**
	 * Массив с цветами
	 * @type {Array}
	 */
	colors: IColorStructure[] = [];

	constructor(private colorsService: ColorsService) {

	}

	/**
	 * Инициалзация
	 */
	ngOnInit(): void {

		this.getColors();
		this.colorsService.selectedColors = {id: this.baseColor.toString(), label: this.baseColor.toString(), value: this.baseColor};
	}

	/**
	 * Функция получения цветов
	 */
	private getColors(): void {

		this.colors = this.colorsService.getColors();
	}

	/**
	 * Функция обработки изменения количества цветов
	 * @param selectedColors {string}
	 */
	colorsChange(selectedColors: string): void {

		this.selectedColor.emit(parseInt(selectedColors, 10));
		this.colorsService.selectedColors = {id: selectedColors, label: selectedColors, value: parseInt(selectedColors, 10)};
	}
}
