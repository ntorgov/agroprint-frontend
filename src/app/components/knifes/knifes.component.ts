import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Knife} from '../../models/knife';
import {KnifesService} from '../../services/knifes.service';

@Component({
	selector: 'app-knifes',
	templateUrl: './knifes.component.html',
	styleUrls: ['./knifes.component.css']
})
export class KnifesComponent implements OnInit, OnChanges {

	label = 'Размер этикетки, мм';

	/**
	 * Массив ножей
	 * @type {Array}
	 */
	knifes: Knife[] = [];
	errorMessage: string;
	defaultKnife: string;

	@Input() selectedShape: string;
	@Output() selectedKnife = new EventEmitter();

	constructor(private knifesService: KnifesService) {
	}

	ngOnInit() {
		// this.getKnifes();
	}

	private getKnifes() {

		this.knifesService.getKnifes(this.selectedShape).subscribe(
			data => this.knifes = data,
			error => this.errorMessage = <any>error,
			() => {
				if (this.knifes && this.knifes.length > 0) {
					this.defaultKnife = this.knifes[0]._id;
					this.selectedKnife.emit(this.knifes[0]._id);
					this.knifesService.selectedKnife = {id: this.knifes[0]._id, label: this.compileKnifeLabel(this.knifes[0])};
				}
			});
	}

	/**
	 * Функция создания подписи нажа из объекта
	 * @param knifeObject
	 * @returns {string}
	 */
	private compileKnifeLabel(knifeObject: Knife): string {

		return `${knifeObject.length}x${knifeObject.width} [№${knifeObject.knifeNumber}]`;
	}

	/**
	 * Функция отслеживает состояние входной переменной с формой ножа
	 * В случае изменения - производит обновление списка ножей
	 * @param changes
	 */
	ngOnChanges(changes: SimpleChanges) {

		if (changes) {

			for (const propName in changes) {

				if (propName === 'selectedShape') {

					if (this.selectedShape !== '') {

						this.knifes = [];
						this.getKnifes();
					}
				}
			}
		}
	}

	/**
	 * Функция изменения выбранного пантона
	 * @param selectedKnifeId
	 */
	knifeChange(selectedKnifeId: string): void {

		let currentLabel: string;
		if (this.knifes && this.knifes.length > 0) {

			for (let counter = 0; counter < this.knifes.length; counter++) {

				if (this.knifes[counter]._id === selectedKnifeId) {

					currentLabel = this.compileKnifeLabel(this.knifes[counter]);
				}
			}
			this.selectedKnife.emit(selectedKnifeId);
			this.knifesService.selectedKnife = {id: selectedKnifeId, label: currentLabel};
		}
	}
}
