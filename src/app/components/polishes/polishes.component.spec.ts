import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PolishesComponent} from './polishes.component';

describe('PolishesComponent', () => {
	let component: PolishesComponent;
	let fixture: ComponentFixture<PolishesComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [PolishesComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PolishesComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
