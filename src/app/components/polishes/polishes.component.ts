/// <reference path='../../models/polish.ts' />

import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {PolishesService} from '../../services/polishes.service';
import {Polish} from '../../models/polish';

@Component({
	selector: 'app-polishes',
	templateUrl: './polishes.component.html',
	styleUrls: ['./polishes.component.css']
})
export class PolishesComponent implements OnInit {

	label = 'Лакировка';
	polishes: Polish[] = [];
	errorMessage = '';
	defaultPolish: string;

	@Output() selectedPolish = new EventEmitter();


	constructor(private polishesService: PolishesService) {
	}

	ngOnInit() {

		this.getPolishes();
	}

	/**
	 * Функция получения лаков
	 */
	private getPolishes(): void {

		this.polishesService.getPolishes().subscribe(
			data => this.polishes = data,
			error => this.errorMessage = <any>error,
			() => {
				if (this.polishes && (this.polishes.length > 0)) {
					this.defaultPolish = this.polishes[0]._id;
					this.selectedPolish.emit(this.polishes[0]._id);
					this.polishesService.selectedPolish = {id: this.polishes[0]._id, label: this.polishes[0].label};
				}
			});
	}

	polishesChange(selectedPolishId): void {

		let currentLabel: string;

		if (this.polishes && this.polishes.length > 0) {

			for (let counter = 0; counter < this.polishes.length; counter++) {

				if (this.polishes[counter]._id === selectedPolishId) {

					currentLabel = this.polishes[counter].label;
				}
			}

			this.selectedPolish.emit(selectedPolishId);
			this.polishesService.selectedPolish = {id: selectedPolishId, label: currentLabel};
		}
	}
}
