import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Http, RequestOptions, Headers} from '@angular/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

// Подключение сервисов. Их дохрена
import {PaymentsService} from '../../services/payments.service';
import {DeliveriesService} from '../../services/deliveries.service';
import {MaterialsService} from '../../services/materials.service';
import {PolishesService} from '../../services/polishes.service';
import {ShapesService} from '../../services/shapes.service';
import {KnifesService} from '../../services/knifes.service';
import {ColorsService} from '../../services/colors.service';
import {PantonesService} from '../../services/pantones.service';
import {QuantitiesService} from '../../services/quantities.service';
import {CalculatorService} from '../../services/calculator.service';
import {KindsService} from '../../services/kinds.service';

// import {Request} from 'request';

@Component({
	selector: 'app-order',
	templateUrl: './order.component.html',
	styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

	/**
	 * Подписи для форм
	 */
	labels = {
		customer: {label: 'Заказчик', placeholder: 'Заказчик'},
		phone: {label: 'Телефон', placeholder: 'Телефон'},
		email: {label: 'E-mail', placeholder: 'E-mail'},
		requisites: {label: 'Реквизиты', placeholder: 'Реквизиты'},
		layout: {label: 'Макет', placeholder: 'Макет'},
		payments: {label: 'Способ оплаты', placeholder: 'Способ оплаты'},
		delivery: {label: 'Доставка', placeholder: 'Доставка'}
	};

	formData: FormData;

	/**
	 * Методы платежа
	 */
	paymentsMethods;

	/**
	 * Методы доставки
	 */
	deliveryMethods;


	constructor(private http: Http,
	            private router: Router,
	            private paymentsService: PaymentsService,
	            private deliveryService: DeliveriesService,
	            private materialsService: MaterialsService,
	            private polishesService: PolishesService,
	            private shapesService: ShapesService,
	            private knifesService: KnifesService,
	            private colorsService: ColorsService,
	            private pantonesService: PantonesService,
	            private quantitiesService: QuantitiesService,
	            private calculatorService: CalculatorService,
	            private kindsService: KindsService) {

	}

	ngOnInit() {

		this.paymentsMethods = this.paymentsService.getPaymentMethods();
		this.deliveryMethods = this.deliveryService.getDeliveryMethods();

		this.formData = new FormData();

		if (!this.calculatorService.calculatorData) {

			this.router.navigateByUrl('/calculator');
		}
	}

	fileChange(event) {

		const fileList: FileList = event.target.files;

		if (fileList.length > 0) {
			const file: File = fileList[0];
			// let formData: FormData = new FormData();
			this.formData.append('layout', file, file.name);

		}
	}

	onSubmit(formData: NgForm) {

		const headers = new Headers();

		headers.append('Content-Type', 'multipart/form-data-encoded');
		headers.append('Accept', 'application/json');
		const options = new RequestOptions({headers: headers});

		// Заполнение данных формы
		this.formData.append('customer', formData.value.customer);
		this.formData.append('phone', formData.value.phone);
		this.formData.append('email', formData.value.email);
		this.formData.append('requisites', formData.value.requisites);
		this.formData.append('payments', formData.value.payments);
		this.formData.append('delivery', formData.value.delivery);

		// Заполнение данных заказа
		this.formData.append('shapeId', this.shapesService.selectedShape.id);
		this.formData.append('shapeLabel', this.shapesService.selectedShape.label);
		this.formData.append('knifeId', this.knifesService.selectedKnife.id);
		this.formData.append('knifeLabel', this.knifesService.selectedKnife.label);
		this.formData.append('materialId', this.materialsService.selectedMaterial.id);
		this.formData.append('materialLabel', this.materialsService.selectedMaterial.label);
		this.formData.append('polishId', this.polishesService.selectedPolish.id);
		this.formData.append('polishLabel', this.polishesService.selectedPolish.label);
		this.formData.append('colorId', this.colorsService.selectedColors.id);
		this.formData.append('colorLabel', this.colorsService.selectedColors.label);
		this.formData.append('pantoneId', this.pantonesService.selectedPantone.id);
		this.formData.append('pantoneLabel', this.pantonesService.selectedPantone.label);
		this.formData.append('quantityId', this.quantitiesService.selectedQuantity.id);
		this.formData.append('quantityLabel', this.quantitiesService.selectedQuantity.label);
		this.formData.append('kindsId', this.kindsService.selectKinds.id);
		this.formData.append('kindsLabel', this.kindsService.selectKinds.label);
		this.formData.append('sum', this.calculatorService.calculatorData.TotalCost);

		const headers2 = new Headers();

		headers.append('Content-Type', 'application/x-www-form-urlencoded');
		const options2 = new RequestOptions({headers: headers2});

		this.http.post('mailer.php', this.formData, options2)
			.map(res => res.json())
			.catch(error => Observable.throw(error))
			.subscribe(
				data => console.log('success'),
				error => console.log(error),
				() => {
					this.router.navigateByUrl('/thanks');
				}
			);

	}
}
