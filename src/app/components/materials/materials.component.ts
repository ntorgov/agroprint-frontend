import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Material} from '../../models/material';
import {MaterialsService} from '../../services/materials.service';

@Component({
	selector: 'app-materials',
	templateUrl: './materials.component.html',
	styleUrls: ['./materials.component.css']
})
export class MaterialsComponent implements OnInit {

	label = 'Материал';
	materials: Material[] = [];
	errorMessage = '';
	defaultMaterial = '';

	@Output() selectedMaterial = new EventEmitter();

	constructor(private materialsService: MaterialsService) {
	}

	ngOnInit() {

		this.getMaterials();
	}

	/**
	 * Функция получения материалов
	 */
	private getMaterials(): void {

		this.materialsService.getMaterials().subscribe(
			data => this.materials = data,
			error => this.errorMessage = <any>error,
			() => {
				if (this.materials && (this.materials.length > 0)) {

					this.defaultMaterial = this.materials[0]._id;
					this.selectedMaterial.emit(this.materials[0]._id);
					this.materialsService.selectedMaterial = {id: this.materials[0]._id, label: this.materials[0].title};
				}
			});
	}

	/**
	 * Обработчик события изменения материала
	 * @param selectedMaterial {string}
	 */
	materialsChange(selectedMaterialId): void {

		let currentLabel: string;
		if (this.materials && this.materials.length > 0) {

			for (let counter = 0; counter < this.materials.length; counter++) {

				if (this.materials[counter]._id === selectedMaterialId) {

					currentLabel = this.materials[counter].title;
				}
			}

			this.selectedMaterial.emit(selectedMaterialId);
			this.materialsService.selectedMaterial = {id: selectedMaterialId, label: currentLabel};
		}
	}

}
