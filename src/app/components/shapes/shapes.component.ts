import {Component, EventEmitter, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Shape} from '../../models/shape';
import {ShapesService} from '../../services/shapes.service';

@Component({
	selector: 'app-shapes',
	templateUrl: './shapes.component.html',
	styleUrls: ['./shapes.component.css']
})
export class ShapesComponent implements OnInit, OnChanges {

	label = 'Форма этикетки';

	/**
	 * Массив с формами
	 * @type {Array}
	 */
	shapes: Shape[] = [];
	errorMessage = '';
	defaultShape: string;

	@Output() selectedShape = new EventEmitter();

	constructor(private shapesService: ShapesService) {
	}

	ngOnInit() {

		this.getShapes();
	}

	/**
	 * Функция получения лаков
	 */
	private getShapes(): void {

		this.shapesService.getShapes().subscribe(
			data => this.shapes = data,
			error => this.errorMessage = <any>error,
			() => {
				this.defaultShape = this.shapes[0]._id;
				this.selectedShape.emit(this.shapes[0]._id);
				this.shapesService.selectedShape = {id: this.shapes[0]._id, label: this.shapes[0].title};
			});
	}

	/**
	 * Обработчик изменения формы ножа
	 * @param selectedShapeId
	 */
	shapesChange(selectedShapeId): void {

		let currentLabel: string;

		if (this.shapes && this.shapes.length > 0) {

			for (let counter = 0; counter < this.shapes.length; counter++) {

				if (this.shapes[counter]._id === selectedShapeId) {

					currentLabel = this.shapes[counter].title;
				}
			}
			this.selectedShape.emit(selectedShapeId);
			this.shapesService.selectedShape = {id: selectedShapeId, label: currentLabel};
		}
	}

	/**
	 * Функция отслеживает состояние входной переменной с формой ножа
	 * В случае изменения - производит обновление списка ножей
	 * @param changes
	 */
	ngOnChanges(changes: SimpleChanges) {

		if (changes) {

			for (const propName in changes) {

				if (propName === 'shapes') {

					this.defaultShape = this.shapes[0]._id;
				}
			}
		}
	}
}
