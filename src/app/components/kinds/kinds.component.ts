import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {KindsService} from '../../services/kinds.service';

@Component({
	selector: 'app-kinds',
	templateUrl: './kinds.component.html',
	styleUrls: ['./kinds.component.css']
})
export class KindsComponent implements OnInit {

	label = 'Количество видов';

	@Output() selectedKind = new EventEmitter();
	/**
	 * Массив с видами
	 * @type {IKindsStructure[]}
	 */
	kinds: IKindsStructure[] = [];

	/**
	 * Значение по умолчанию
	 */
	defaultKind: number;

	constructor(private kindsService: KindsService) {

		this.defaultKind = 1;
		this.kindsService.selectKinds = {id: this.defaultKind.toString(), label: this.defaultKind.toString(), value: this.defaultKind};
	}

	ngOnInit(): void {

		this.kinds = this.kindsService.getKinds();
	}

	kindsChange(selectedKinds: string): void {

		this.selectedKind.emit(parseInt(selectedKinds, 10));
		this.kindsService.selectKinds = {id: selectedKinds, label: selectedKinds, value: parseInt(selectedKinds, 10)};
	}
}
