import {Component, OnInit} from '@angular/core';
import {Calculator} from 'app/models/calculator';
import {Router} from '@angular/router';
import {CalculatorService} from '../../services/calculator.service';

@Component({
	selector: 'app-calculator',
	templateUrl: './calculator.component.html',
	styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {

	/**
	 * Объект заказа
	 * @type {IOrderStructure}
	 */
	order: IOrderStructure = {
		colors: 1,
		pantones: 0,
		kinds: 1,
		quantity: 1000,
		polish: '',
		material: '',
		shape: '',
		knife: ''
	};

	calculator: Calculator;

	constructor(private router: Router,
	            private calculatorService: CalculatorService) {
	}

	ngOnInit() {
	}

	/**
	 * Обработчик изменения количества цветов
	 * @param event {number}
	 */
	public handleChangeColor(event: number): void {

		this.order.colors = event;
		this.calculator = this.calculatorService.calculatorData;
	}

	/**
	 * Обработчик изменения количества пантонов
	 * @param event {number}
	 */
	public handleChangePantone(event: number): void {

		this.order.pantones = event;
		this.calculator = this.calculatorService.calculatorData;
	}

	/**
	 * Обработчик изменения колчества видов
	 * @param event {number}
	 */
	public handleChangeKind(event: number): void {

		this.order.kinds = event;
		this.calculator = this.calculatorService.calculatorData;
	}

	/**
	 * Обработчик изменения тиража
	 * @param event {string}
	 */
	public handleChangeQuantity(event: string): void {

		this.order.quantity = parseInt(event, 10);
		this.calculator = this.calculatorService.calculatorData;
	}

	/**
	 * Обработчик изменения лака
	 * @param event {string}
	 */
	public handleChangePolish(event: string): void {

		this.order.polish = event;
		this.calculator = this.calculatorService.calculatorData;
	}

	/**
	 * Обработчик изменения материала
	 * @param event {string}
	 */
	public handleChangeMaterial(event: string): void {

		this.order.material = event;
		this.calculator = this.calculatorService.calculatorData;
	}

	/**
	 * Обработчик изменения формы ножа
	 * @param event {string}
	 */
	public handleChangeShape(event: string): void {

		this.order.shape = event;
		this.calculator = this.calculatorService.calculatorData;
	}

	/**
	 * Обработчик изменения ножа
	 * @param event {number}
	 */
	public handleChangeKnife(event: string): void {

		this.order.knife = event;
		this.calculator = this.calculatorService.calculatorData;
	}

	/**
	 * Обработчик изменения результатов расчета
	 * @param event
	 */
	public handleChangeCalculatorResult(event): void {

		this.calculator = event;
		this.calculator = this.calculatorService.calculatorData;
	}

	/**
	 * Кнопка нажатия заказа
	 */
	public goOrder(): void {
		this.router.navigateByUrl('/order');
	}
}
