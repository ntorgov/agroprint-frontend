/**
 * Интерфейс структуры тиража
 */
interface IQuantityStructure {

	/**
	 * Значение
	 */
	value: number;

	/**
	 * Подпись
	 */
	label: string;

	/**
	 * Является ли значением по умолчанию
	 */
	isDefault?: boolean;
}
