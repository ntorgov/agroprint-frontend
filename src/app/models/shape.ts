export class Shape {

	constructor(public _id: string,
	            public title: string,
	            public value: number,
	            public sortOrder?: number) {

	}
}
