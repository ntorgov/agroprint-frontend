/// <reference path='./calculator.d.ts' />

export class Calculator implements ICalculatorStructure {

	constructor(public TotalCost: string,
	            public StickerCost: string,
	            public TotalTime: string,
	            public TotalLength: string,
	            public TotalSquare: string,
	            public TimeToReroll: string,
	            public ClearLength: string,
	            public SecondPrintingCost: string,
	            public SecondPrintingStickerCost: string,
	            public FlexFormCost: string,
	            public TotalWeight: string) {

	}
}
