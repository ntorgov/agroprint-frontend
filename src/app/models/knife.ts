export class Knife {

	constructor(public _id: string,
	            public length: number,
	            public width: number,
	            public knifeNumber: number,
	            public quantityPerMaterial: number,
	            public lengthBetweenStampsByRapport: number,
	            public radius: number) {

	}
}
