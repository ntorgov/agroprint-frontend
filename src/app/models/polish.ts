export class Polish {

	constructor(public _id: string,
	            public label: string,
	            public value: number,
	            public sortOrder: number) {

	}
}
