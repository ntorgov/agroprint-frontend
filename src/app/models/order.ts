/// <reference path='./order.d.ts' />

export class Order implements IOrderStructure {

	public shape: string;

	/**
	 * ID ножа
	 * @type {string}
	 */
	public knife: string;
	public colors: number;
	public pantones: number;
	public kinds: number;
	public quantity: number;
	public polish: string;
	public material: string;

	constructor(parameters: IOrderStructure) {
		this.shape = parameters.shape;
		this.knife = parameters.knife;
		this.colors = parameters.colors;
		this.pantones = parameters.pantones;
		this.kinds = parameters.kinds;
		this.quantity = parameters.quantity;
		this.polish = parameters.polish;
		this.material = parameters.material;
	}
}
