interface ICalculatorStructure {

	/**
	 * Полная стоимость
	 */
	TotalCost: string;

	/**
	 * Стоимость за этикетку
	 */
	StickerCost: string;

	/**
	 * Полное время печати
	 */
	TotalTime: string;

	/**
	 * Полная длина тиража
	 */
	TotalLength: string;

	/**
	 * Площадь печати
	 */
	TotalSquare: string;

	/**
	 * Время на перемотку
	 */
	TimeToReroll: string;

	/**
	 * Чистая длина печати
	 */
	ClearLength: string;

	/**
	 * Стоимость повторного тиража
	 */
	SecondPrintingCost: string;

	/**
	 * Цена за этикетку при повторном тираже
	 */
	SecondPrintingStickerCost: string;

	/**
	 * Стоимость флексоформ
	 */
	FlexFormCost: string;

	/**
	 * Примерный вес тиража
	 */
	TotalWeight: string;
}
