interface IOrderStructure {
	shape: string;

	/**
	 * ID ножа
	 * @type {string}
	 */
	knife: string;
	colors: number;
	pantones: number;
	kinds: number;
	quantity: number;
	polish: string;
	material: string;
}
