/**
 * Интерфейс структуры цвета
 */
interface IColorStructure {

	/**
	 * Значение
	 */
	value: number;

	/**
	 * Подпись
	 */
	label: string;

	/**
	 * Является ли значением по умолчанию
	 */
	isDefault?: boolean;
}
