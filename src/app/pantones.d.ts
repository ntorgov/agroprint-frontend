/**
 * Интерфейс структуры пантона
 */
interface IPantoneStructure {

	/**
	 * Значение
	 */
	value: number;

	/**
	 * Подпись
	 */
	label: string;

	/**
	 * Является ли значением по умолчанию
	 */
	isDefault?: boolean;
}
