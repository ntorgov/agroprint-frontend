/**
 * Интерфейс структуры видов
 */
interface IKindsStructure {

	/**
	 * Значение
	 */
	value: number;

	/**
	 * Подпись
	 */
	label: string;

	/**
	 * Является ли значением по умолчанию
	 */
	isDefault?: boolean;
}
