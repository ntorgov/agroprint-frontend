/// <reference path='./models/order.d.ts' />

import {Component} from '@angular/core';
import {ColorsService} from './services/colors.service';
import {PantonesService} from './services/pantones.service';
import {KindsService} from './services/kinds.service';
import {QuantitiesService} from './services/quantities.service';
import {PolishesService} from './services/polishes.service';
import {MaterialsService} from './services/materials.service';
import {ShapesService} from './services/shapes.service';
import {KnifesService} from './services/knifes.service';
import {PaymentsService} from './services/payments.service';
import {DeliveriesService} from './services/deliveries.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	providers: [ColorsService,
		PantonesService,
		KindsService,
		QuantitiesService,
		PolishesService,
		MaterialsService,
		ShapesService,
		KnifesService,
		PaymentsService,
		DeliveriesService]
})
export class AppComponent {

	/**
	 * Конструктор. Ничего особенного
	 */
	constructor() {
	}
}
