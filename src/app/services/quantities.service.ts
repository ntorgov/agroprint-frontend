/// <reference path='../quantities.d.ts' />

import {Injectable} from '@angular/core';

@Injectable()
export class QuantitiesService {

	private quantityArray: { start: number, end: number, step: number }[] = [
		{
			start: 1000,
			end: 10000,
			step: 500
		},
		{
			start: 11000,
			end: 100000,
			step: 1000
		},
		{
			start: 110000,
			end: 500000,
			step: 10000
		},
		{
			start: 550000,
			end: 1000000,
			step: 50000
		}
	];

	/**
	 * Выбранный тираж
	 */
	private _selectedQuantity: { id?: string, label?: string, value: number };

	/**
	 * Массив с тиражами
	 * @type {IPantoneStructure[]}
	 */
	quantities: IQuantityStructure[] = [];

	/**
	 * Гетер для тиража
	 * @returns {{id?: string, label?: string, value: number}}
	 */
	public get selectedQuantity(): { id?: string; label?: string; value: number } {
		return this._selectedQuantity;
	}

	/**
	 * Сетер для тиража
	 * @param value
	 */
	public set selectedQuantity(value: { id?: string; label?: string; value: number }) {
		this._selectedQuantity = value;
	}

	constructor() {
	}

	/**
	 * Функция создания списка тиражей
	 * @returns {IQuantityStructure[]}
	 */
	getQuantities(): IQuantityStructure[] {

		/**
		 * Счетчик
		 * @type {number}
		 */
		let n: number;

		for (let qtyCounter = 0; qtyCounter < this.quantityArray.length; qtyCounter++) {

			for (n = this.quantityArray[qtyCounter].start; n <= this.quantityArray[qtyCounter].end; n += this.quantityArray[qtyCounter].step) {

				this.quantities.push({
					value: n,
					label: n.toString()
				});
			}
		}

		return this.quantities;
	}
}
