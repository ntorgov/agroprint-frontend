import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import {Knife} from '../models/knife';

@Injectable()
export class KnifesService {

	private knifesUrl = 'gate.php?request=knifes&data=true';

	private _selectedKnife: { id: string, label?: string };

	/**
	 * Гетер для выбранного ножа
	 * @returns {{id: string, label?: string}}
	 */
	public get selectedKnife(): { id: string; label?: string } {
		return this._selectedKnife;
	}

	/**
	 * Сетер для выбранного ножа
	 * @param value
	 */
	public set selectedKnife(value: { id: string; label?: string }) {
		this._selectedKnife = value;
	}

	constructor(private http: Http) {
	}

	getKnifes(selectedShape: string): Observable<Knife[]> {

		let result;

		if (location.hostname === 'localhost' || location.hostname === '127.0.0.1') {

			this.knifesUrl = 'assets/mock-data/knifes_' + selectedShape + '.json';

			result = this.http.get(this.knifesUrl).map(this.extractData).catch(this.handleError);

		} else {

			result = this.http.post(this.knifesUrl, selectedShape).map(this.extractData).catch(this.handleError);
		}

		return result;
	}

	private extractData(res: Response) {

		const body = res.json();
		return body.data || {};
	}

	private handleError(error: Response | any) {
		// In a real world app, you might use a remote logging infrastructure

		let errMsg: string;
		if (error instanceof Response) {
			const body = error.json() || '';
			const err = body.error || JSON.stringify(body);
			errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
		} else {
			errMsg = error.message ? error.message : error.toString();
		}
		// console.error(errMsg);
		return Observable.throw(errMsg);
	}
}
