import {TestBed, inject} from '@angular/core/testing';

import {QuantitiesService} from '../quantities.service';

describe('QuantitiesService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [QuantitiesService]
		});
	});

	it('should ...', inject([QuantitiesService], (service: QuantitiesService) => {
		expect(service).toBeTruthy();
	}));
});
