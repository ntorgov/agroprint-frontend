import {TestBed, inject} from '@angular/core/testing';

import {PantonesService} from '../pantones.service';

describe('PantonesService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [PantonesService]
		});
	});

	it('should ...', inject([PantonesService], (service: PantonesService) => {
		expect(service).toBeTruthy();
	}));
});
