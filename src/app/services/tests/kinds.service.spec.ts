import {TestBed, inject} from '@angular/core/testing';

import {KindsService} from '../kinds.service';

describe('KindsService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [KindsService]
		});
	});

	it('should ...', inject([KindsService], (service: KindsService) => {
		expect(service).toBeTruthy();
	}));
});
