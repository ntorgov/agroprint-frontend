import { TestBed, inject } from '@angular/core/testing';

import { PolishesService } from '../polishes.service';

describe('PolishesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PolishesService]
    });
  });

  it('should ...', inject([PolishesService], (service: PolishesService) => {
    expect(service).toBeTruthy();
  }));
});
