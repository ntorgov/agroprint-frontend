import { TestBed, inject } from '@angular/core/testing';

import { KnifesService } from '../knifes.service';

describe('KnifesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KnifesService]
    });
  });

  it('should ...', inject([KnifesService], (service: KnifesService) => {
    expect(service).toBeTruthy();
  }));
});
