/// <reference path='../models/polish.ts' />

import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import {Polish} from '../models/polish';

@Injectable()
export class PolishesService {

	private polishesUrl = 'gate.php?request=polishes';

	private _selectedPolish: { id: string, label?: string };

	public get selectedPolish(): { id: string; label?: string } {
		return this._selectedPolish;
	}

	public set selectedPolish(value: { id: string; label?: string }) {
		this._selectedPolish = value;
	}

	/**
	 * Кон, мать его, структор
	 * @param http
	 */
	constructor(private http: Http) {

		if (location.hostname === 'localhost' || location.hostname === '127.0.0.1') {

			this.polishesUrl = 'assets/mock-data/polishes.json';
		}
	}

	getPolishes(): Observable<Polish[]> {

		const result = this.http.get(this.polishesUrl).map(this.extractData).catch(this.handleError);

		return result;
	}

	private extractData(res: Response) {

		const body = res.json();
		return body.data || {};
	}

	private handleError(error: Response | any) {
		// In a real world app, you might use a remote logging infrastructure

		let errMsg: string;
		if (error instanceof Response) {
			const body = error.json() || '';
			const err = body.error || JSON.stringify(body);
			errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
		} else {
			errMsg = error.message ? error.message : error.toString();
		}
		// console.error(errMsg);
		return Observable.throw(errMsg);
	}
}
