/// <reference path='../pantones.d.ts' />

import {Injectable} from '@angular/core';

@Injectable()
export class PantonesService {

	/**
	 * Массив с цветами
	 * @type {IPantoneStructure[]}
	 */
	pantones: IPantoneStructure[] = [];

	/**
	 * Выбранный пантон
	 */
	private _selectedPantone: { id?: string, value: number, label?: string };

	/**
	 * Гетер для выбранного пантона
	 * @returns {{id?: string, value: number, label?: string}}
	 */
	public get selectedPantone(): { id?: string; value: number; label?: string } {
		return this._selectedPantone;
	}

	/**
	 * Сетер для выбранного пантона
	 * @param value
	 */
	public set selectedPantone(value: { id?: string; value: number; label?: string }) {
		this._selectedPantone = value;
	}

	constructor() {
	}

	/**
	 * Функция получения списка пантонов для расчета
	 * @param maximumPantones {number} максимально возможное количество пантонов
	 * @returns {IColorStructure[]}
	 */
	getPantones(maximumPantones: number): IPantoneStructure[] {

		let counter: number;

		this.pantones = [];

		for (counter = 0; counter <= maximumPantones; counter++) {

			this.pantones.push({value: counter, label: counter.toString()});
		}

		return this.pantones;
	}
}
