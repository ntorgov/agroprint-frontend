interface IMaterialsService {

	selectedMaterial: {
		id: string,
		label?: string
	};

	getMaterials(): any;
}
