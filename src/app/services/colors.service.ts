/// <reference path='../colors.d.ts' />

import {Injectable} from '@angular/core';

@Injectable()
export class ColorsService {

	/**
	 * Массив с цветами
	 * @type {Array}
	 */
	public colors: IColorStructure[] = [];

	/**
	 * Максимально возможное количество цветов
	 * @type {number}
	 */
	public maxColors = 6;

	/**
	 * Выбранное количество цветов
	 */
	private _selectedColors: { id?: string, value: number, label?: string };

	/**
	 * Гетер для количества цветов
	 * @returns {{id?: string, value: number, label?: string}}
	 */
	public get selectedColors(): { id?: string; value: number; label?: string } {
		return this._selectedColors;
	}

	/**
	 * Сетер для количества цветов
	 * @param value
	 */
	public set selectedColors(value: { id?: string; value: number; label?: string }) {
		this._selectedColors = value;
	}

	constructor() {
	}

	/**
	 * Функция получения списка цветов для расчета
	 * @returns {IColorStructure[]}
	 */
	getColors(): IColorStructure[] {

		let counter: number;

		for (counter = 0; counter <= this.maxColors; counter++) {

			if (counter === 0) {
				this.colors.push({value: counter, label: 'Без печати'});
			} else {
				this.colors.push({value: counter, label: counter.toString()});
			}
		}

		return this.colors;
	}

	private setMaxColors(colorsNumber: number): void {

		this.maxColors = colorsNumber;
	}
}
