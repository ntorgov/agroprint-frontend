/// <reference path="./materials.service.d.ts"/>

import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import {Material} from '../models/material';

@Injectable()
export class MaterialsService implements IMaterialsService {

	private materialsUrl = 'gate.php?request=materials';

	/**
	 * Выбранный материал
	 */
	private _selectedMaterial: { id: string, label?: string };

	/**
	 * Гетер для выбранного материала
	 * @returns {{id: string, label?: string}}
	 */
	public get selectedMaterial(): { id: string; label?: string } {
		return this._selectedMaterial;
	}

	/**
	 * Сетер для выбранного материала
	 * @param value
	 */
	public set selectedMaterial(value: { id: string; label?: string }) {
		this._selectedMaterial = value;
	}

	/**
	 * Конструктор, как видно из названия
	 * @param http
	 */
	constructor(private http: Http) {

		if (location.hostname === 'localhost' || location.hostname === '127.0.0.1') {

			this.materialsUrl = 'assets/mock-data/materials.json';
		}
	}

	getMaterials(): Observable<Material[]> {
		const result = this.http.get(this.materialsUrl).map(this.extractData).catch(this.handleError);

		return result;
	}

	private extractData(res: Response) {

		const body = res.json();
		return body.data || {};
	}

	private handleError(error: Response | any) {
		// In a real world app, you might use a remote logging infrastructure

		let errMsg: string;
		if (error instanceof Response) {
			const body = error.json() || '';
			const err = body.error || JSON.stringify(body);
			errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
		} else {
			errMsg = error.message ? error.message : error.toString();
		}
		// console.error(errMsg);
		return Observable.throw(errMsg);
	}
}
