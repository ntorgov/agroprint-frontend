import {Injectable} from '@angular/core';

@Injectable()
export class PaymentsService {

	private paymentsMethods = [
		{
			customerType: 'Для юридических лиц',
			methods: [
				{
					value: 2,
					label: 'Безналичный расчет (выставляется счет)'
				}]
		},
		{
			customerType: 'Для физических лиц',
			methods: [
				{
					value: 1,
					label: 'Наличными'
				},
				{
					value: 5,
					label: 'Банковская карта Сбербанк перевод на карту номер: 4276 8381 4552 9063'
				},
				{
					value: 3,
					label: 'Яндекс.Деньги перевод на номер: 4100147821594'
				},
				{
					value: 6,
					label: 'QIWI.Терминалы перевод на номер: +79255147959'
				}
			]
		}
	];

	constructor() {
	}

	getPaymentMethods() {

		return this.paymentsMethods;
	}
}
