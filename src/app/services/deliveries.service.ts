import {Injectable} from '@angular/core';

@Injectable()
export class DeliveriesService {

	private deliveriesMethods = [
		{
			value: 1,
			label: 'Самовывоз (бесплатно)'
		},
		{
			value: 2,
			label: 'Доставка по Москве — 350 руб.'
		},
		{
			value: 6,
			label: 'Доставка по России — 450-1200 руб. по тарифам транспортной компании "КурьерСервисЭкспресс"'
		}
	];

	constructor() {
	}

	getDeliveryMethods() {

		return this.deliveriesMethods;
	}
}
