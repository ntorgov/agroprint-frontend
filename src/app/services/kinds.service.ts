/// <reference path='../kinds.d.ts' />

import {Injectable} from '@angular/core';

@Injectable()
export class KindsService {

	/**
	 * Массив с видами
	 * @type {IKindsStructure[]}
	 */
	kinds: IKindsStructure[] = [];

	/**
	 * Максимальное количество видов
	 * @type {number}
	 */
	maximumKinds = 25;

	/**
	 * Выбранное количество видов
	 */
	private _selectKinds: { id?: string, value: number, label?: string };

	/**
	 * Гетер для количества видов
	 * @returns {{id?: string, value: number, label?: string}}
	 */
	public get selectKinds(): { id?: string; value: number; label?: string } {
		return this._selectKinds;
	}

	/**
	 * Сетер для количества видов
	 * @param value
	 */
	public set selectKinds(value: { id?: string; value: number; label?: string }) {
		this._selectKinds = value;
	}

	constructor() {
	}

	/**
	 * Функция получения списка видов
	 * @returns {IKindsStructure[]}
	 */
	getKinds(): IKindsStructure[] {

		let counter: number;

		this.kinds = [];

		for (counter = 0; counter <= this.maximumKinds; counter++) {

			this.kinds.push({value: counter, label: counter.toString()});
		}

		return this.kinds;
	}
}
