/// <reference path='../models/shape.ts' />

import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import {Shape} from '../models/shape';

@Injectable()
export class ShapesService {

	private shapesUrl = 'gate.php?request=shapes';

	private _selectedShape: { id: string, label?: string };

	/**
	 * Гетер выбранной формы
	 * @returns {{id: string, label?: string}}
	 */
	public get selectedShape(): { id: string; label?: string } {

		return this._selectedShape;
	}

	/**
	 * Сетер выбранной формы
	 * @param value
	 */
	public set selectedShape(value: { id: string; label?: string }) {

		this._selectedShape = value;
	}

	constructor(private http: Http) {

		if (location.hostname === 'localhost' || location.hostname === '127.0.0.1') {

			this.shapesUrl = 'assets/mock-data/shapes.json';
		}
	}

	getShapes(): Observable<Shape[]> {

		const result = this.http.get(this.shapesUrl).map(this.extractData).catch(this.handleError);

		return result;
	}

	private extractData(res: Response) {

		const body = res.json();
		return body.data || {};
	}

	private handleError(error: Response | any) {
		// In a real world app, you might use a remote logging infrastructure

		let errMsg: string;
		if (error instanceof Response) {
			const body = error.json() || '';
			const err = body.error || JSON.stringify(body);
			errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
		} else {
			errMsg = error.message ? error.message : error.toString();
		}

		return Observable.throw(errMsg);
	}
}
